package com.bussinessrules.OrderPaymentsRules.service.impl;

import com.bussinessrules.OrderPaymentsRules.model.OrderProcessing;
import com.bussinessrules.OrderPaymentsRules.service.OrderProcessingService;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderProcessingServiceImpl implements OrderProcessingService {
    @Autowired
    KieSession kieSession;

    @Override
    public OrderProcessing processOrder(OrderProcessing orderProcessing) {
        try {
            kieSession.insert(orderProcessing);
            kieSession.fireAllRules();

            //should i need to add any business logic either to be validated from db or external messaging system ????? 

        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderProcessing;
    }
}

package com.bussinessrules.OrderPaymentsRules.service;

import com.bussinessrules.OrderPaymentsRules.model.OrderProcessing;

public interface OrderProcessingService {
    OrderProcessing processOrder(OrderProcessing orderProcessing);
}

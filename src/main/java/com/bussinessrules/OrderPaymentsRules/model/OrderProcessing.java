package com.bussinessrules.OrderPaymentsRules.model;

public class OrderProcessing {
    private String nameOfProduct;
    private String status;


    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

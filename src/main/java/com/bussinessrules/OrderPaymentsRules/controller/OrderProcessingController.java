package com.bussinessrules.OrderPaymentsRules.controller;

import com.bussinessrules.OrderPaymentsRules.dto.OrderProcessingDto;
import com.bussinessrules.OrderPaymentsRules.model.OrderProcessing;
import com.bussinessrules.OrderPaymentsRules.service.impl.OrderProcessingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rules")
public class OrderProcessingController {

    @Autowired
    OrderProcessingServiceImpl orderProcessingService;

    @PostMapping(value = "/processOrderForPayment")
    public ResponseEntity<Object> orderProcessing(@RequestBody OrderProcessing orderProcessing) {
        try {
            if (orderProcessing == null || orderProcessing.getNameOfProduct() == null) {
                return new ResponseEntity<>("Please pass in the valid request to process payment", HttpStatus.PRECONDITION_FAILED);
            } else {
                orderProcessing = orderProcessingService.processOrder(orderProcessing);
                OrderProcessingDto orderProcessingDto = new OrderProcessingDto(orderProcessing.getNameOfProduct(), orderProcessing.getStatus());
                return new ResponseEntity<>(orderProcessingDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }
}

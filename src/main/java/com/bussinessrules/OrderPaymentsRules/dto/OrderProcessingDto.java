package com.bussinessrules.OrderPaymentsRules.dto;

public class OrderProcessingDto {
    private String nameOfProduct;
    private String paymentStatus;

    public OrderProcessingDto(String nameOfProduct, String paymentStatus) {
        this.nameOfProduct = nameOfProduct;
        this.paymentStatus = paymentStatus;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}

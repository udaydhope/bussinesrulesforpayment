package com.bussinessrules.OrderPaymentsRules;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderPaymentsRulesApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderPaymentsRulesApplication.class, args);
	}

}

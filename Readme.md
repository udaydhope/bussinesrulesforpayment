I have Developed the Solution for Problem Statement-2 i.e (Business Rules Engine) using Programming Language Java with Proper Spring Boot MVC application by exposing the rest endpoints 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

For the business rules specifically as per the requirement i thought of (Drools) would be the correct choise so i 
have levergaed Drools Engine for applying rules as required (everything was specified in orderPayment.drl file either we can use drl extension
or we can specify in xls file too so that anybody can understand in a readable form ) 

Here by i have used .drl file to apply the rules as specified

Run the application by taking the checkout of this Project and follow the below steps :

1.----> mvn clean install <------- 

2.Go to Resources Folder and copy the application.properties and orderPayment.drl files to Target folder

3.Run the Jar using the command  <--- java -jar OrderPaymentsRules-0.0.1-SNAPSHOT.jar  ---> 

The application should run on the port : 8182 , if any conflict with the port you can change the port as required in application.properties

RestEnd Points attached below for the specified payments to test with Postman or ARC
 
POST - http://localhost:8182/rules/processOrderForPayment

Request :

{
    "nameOfProduct" : "Book"
}

Response :

{
    "nameOfProduct": "Book",
    "paymentStatus": "The Packaging is generated and will attached to shipment soon and Sent to royality department"
}

This would be the response and should i need to process this paymentStatus to any external system ???

Note : Should i need to complete the business logic in service layer to validate the specified rule based on the content given in PDF ??